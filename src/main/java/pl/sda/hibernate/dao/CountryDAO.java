package pl.sda.hibernate.dao;

import org.hibernate.Session;
import pl.sda.hibernate.domain.Country;
import pl.sda.hibernate.domain.Person;

import java.util.List;

public class CountryDAO {

	private Session session;

	public CountryDAO(Session session) {
		this.session = session;
	}

	public Country create(Country country) {
		session.persist(country);
		return country;
	}

	public Country getById(int id) {
		return session.get(Country.class, id);
	}

	public List<Country> getAll() {
		return session.createQuery("from Country").getResultList();
	}
}
