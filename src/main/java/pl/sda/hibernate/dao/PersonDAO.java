package pl.sda.hibernate.dao;

import org.hibernate.Session;
import pl.sda.hibernate.domain.Continent;
import pl.sda.hibernate.domain.Person;

import java.util.List;

public class PersonDAO {

	private Session session;

	public PersonDAO(Session session) {
		this.session = session;
	}

	public Person create(Person person) {
		session.persist(person);
		return person;
	}

	public Person getById(int id) {
		return session.get(Person.class, id);
	}

	public List<Person> getAll() {
		return session.createQuery("from Person").getResultList();
	}

	public List<Person> findByTown(String town) {
		return session.createQuery("from Person p where p.contact.address.town = :town")
				.setParameter("town", town)
				.getResultList();
	}

	public List<Person> findByCountryName(String countryName) {
		return session.createQuery("select p from Person p join p.countries c where c.name = :name")
				.setParameter("name", countryName).getResultList();
	}

	public List<Person> findByContinent(Continent continent) {
		// TODO:
		return null;
	}

	public Person update(Person person) {
		session.save(person);
		return person;
	}

	public Person remove(int id) {
		final Person byId = getById(id);
		if (byId != null) {
			session.remove(byId);
		}
		return byId;
	}
}
