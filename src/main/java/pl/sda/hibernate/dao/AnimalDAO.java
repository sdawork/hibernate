package pl.sda.hibernate.dao;

import org.hibernate.Session;
import pl.sda.hibernate.domain.Animal;
import pl.sda.hibernate.domain.Person;

import java.util.List;

public class AnimalDAO {

	private Session session;

	public AnimalDAO(Session session) {
		this.session = session;
	}

	public Animal create(Animal animal) {
		session.persist(animal);
		return animal;
	}

	public Animal getById(int id) {
		return session.get(Animal.class, id);
	}

	public List<Animal> getAll() {
		return (List<Animal>) session.createQuery("from Animal").getResultList();
	}

	public List<Animal> findByTown(String town) {
		return session.createQuery("from Animal a where a.person.contact.address.town = :town")
				.setParameter("town", town)
				.getResultList();
	}

	public Animal remove(int id) {
		final Animal byId = getById(id);
		if (byId != null) {
			session.remove(byId);
		}
		return byId;
	}

}
