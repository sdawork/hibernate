package pl.sda.hibernate.dao;

import org.hibernate.Session;
import pl.sda.hibernate.domain.Animal;
import pl.sda.hibernate.domain.Contact;

import java.util.List;

public class ContactDAO {

	private Session session;

	public ContactDAO(Session session) {
		this.session = session;
	}

	public Contact create(Contact contact) {
		session.persist(contact);
		return contact;
	}

	public Contact getById(int id) {
		return session.get(Contact.class, id);
	}

	public List<Contact> getAll() {
		return (List<Contact>) session.createQuery("from Contact").getResultList();
	}

}
