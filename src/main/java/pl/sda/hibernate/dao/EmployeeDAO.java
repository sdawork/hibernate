package pl.sda.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.hibernate.olddomain.Employee;
import pl.sda.hibernate.util.HibernateUtil;

import java.util.List;
import java.util.Optional;

public class EmployeeDAO {


	public void create(Employee employee) {
		Transaction transaction = null;

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();

			session.save(employee);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public Employee getById(int id) {
		// TODO:
		return null;
	}

	/**
	 * Hint: session.byId(LITERAL_KLASY_ENCJI).loadOptional(IDENTYFIKATOR_ENCJI)
	 */
	public Optional<Employee> findById(int id) {
		// TODO:
		return null;
	}

	/**
	 * Hint: session.createQuery("from NAZWA_KLASY_ENCJI").getResultList()
	 */
	public List<Employee> findAll() {
		// TODO:
		return null;
	}

	public Employee update(Employee updatedEmployee) {
		// TODO:
		return null;
	}

	public Employee delete(int id) {
		// TODO:
		return null;
	}
}
