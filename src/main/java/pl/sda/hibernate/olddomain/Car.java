package pl.sda.hibernate.olddomain;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "BAZA_SAMOCHODOW")
public class Car {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String brand;
	private LocalDate productionYear;
	@Enumerated(EnumType.STRING)
	private CarType carType;
	@Transient
	private int age;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;


	public Car() {
		//
	}

	public Car(String brand, LocalDate productionYear, CarType carType) {
		this.brand = brand;
		this.productionYear = productionYear;
		this.carType = carType;
	}

	@PrePersist
	public void setUpCreatedDate() {
		createdDate = LocalDateTime.now();
	}

	@PreUpdate
	public void setUpModifiedDate() {
		modifiedDate = LocalDateTime.now();
	}

	@PostLoad
	public void calculateAge() {
		age = (int) ChronoUnit.YEARS.between(productionYear, LocalDate.now());
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Car{" +
				"id=" + id +
				", brand='" + brand + '\'' +
				", productionYear=" + productionYear +
				", carType=" + carType +
				", age=" + age +
				", createdDate=" + createdDate +
				", modified=" + modifiedDate +
				", employee=" + employee +
				'}';
	}
}
