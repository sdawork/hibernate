package pl.sda.hibernate.olddomain;

public enum CarType {
	SEDAN,
	KOMBI,
	CABRIO
}
