package pl.sda.hibernate.olddomain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Note {

	public static final int DEFAULT_PRIORITY = 5;
	public static final int MIN_PRIORITY = 1;
	public static final int MAX_PRIORITY = 5;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private int priority;

	Note() {
	}

	public Note(String title) {
		this(title, DEFAULT_PRIORITY);
	}

	public Note(String title, int priority) {
		this.title = title;
		validateAndSetOrDefault(priority);
	}

	private void validateAndSetOrDefault(int priority) {
		if (priority < MIN_PRIORITY || priority > MAX_PRIORITY) {
			this.priority = DEFAULT_PRIORITY;
		} else {
			this.priority = priority;
		}
	}

	public String getTitle() {
		return title;
	}

	public int getPriority() {
		return priority;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPriority(int priority) {
		validateAndSetOrDefault(priority);
	}

	@Override
	public String toString() {
		return "Note{" +
				"id=" + id +
				", title='" + title + '\'' +
				", priority=" + priority +
				'}';
	}
}
