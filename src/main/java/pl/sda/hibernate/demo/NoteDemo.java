package pl.sda.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.olddomain.Note;
import pl.sda.hibernate.util.HibernateUtil;

import org.hibernate.Transaction;

import java.util.List;

public class NoteDemo {

	public static void main(String[] args) {
		Transaction transaction = null;
		try (
				SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				Session session = sessionFactory.openSession();
		) {
			transaction = session.beginTransaction();
// zapisanie
//			final Note note = new Note("kupić chleb");
//			session.persist(note);

			final List<String> notes = session
					.createQuery("select n.title from Note n where n.priority = :pr")
					.setParameter("pr", 1)
					.getResultList();
			System.out.println("notes = " + notes);

			Long sumOfPriority = (Long) session
					.createQuery("select sum(n.priority) from Note n")
					.getSingleResult();

			System.out.println("sumOfPriority = " + sumOfPriority);

			List<Note> some = session
					.byMultipleIds(Note.class)
					.multiLoad(1, 3, 4);
			System.out.println("some = " + some);

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		}
	}
}
