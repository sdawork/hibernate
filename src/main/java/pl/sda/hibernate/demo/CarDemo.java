package pl.sda.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.olddomain.Employee;
import pl.sda.hibernate.util.HibernateUtil;

public class CarDemo {

	public static void main(String[] args) {
		Transaction transaction = null;
		try (
				SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				Session session = sessionFactory.openSession();
		) {
			transaction = session.beginTransaction();


			//final Employee employee = session.get(Employee.class, 7);
			//final Car car = session.get(Car.class, 3);
			// ustawiamy samochodowi wlasciciela
			//car.setEmployee(employee);

			//final Employee employee = session.get(Employee.class, 7);

//			final List<Car> cars = session
//					.createQuery("from Car where employee.id = :id")
//					.setParameter("id", 7)
//					.getResultList();
//			System.out.println("cars = " + cars);

			//final Car car = session.get(Car.class, 3);
			//car.setEmployee(null);

			// SELECT * FROM Employee WHERE id = 7
			Employee employee = session.get(Employee.class, 7);
			System.out.println(employee);
//
//			System.out.println(employee.getCars());

//			Car car = session.get(Car.class, 1);
//			System.out.println("car = " + car);


			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		}
	}
}
