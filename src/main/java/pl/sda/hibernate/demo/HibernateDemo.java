package pl.sda.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.olddomain.Employee;
import pl.sda.hibernate.util.HibernateUtil;

import java.util.List;

public class HibernateDemo {

	public static void main(String[] args) {

		Transaction transaction = null;
		try (
				SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				Session session = sessionFactory.openSession();
		) {
			transaction = session.beginTransaction();

			// SELECT * FROM EMPLOYEES
			final List<Employee> employees = session
					.createQuery("from Employee where id > :id2")
					.setParameter("id2", 4)
					.getResultList();
			System.out.println("employees = " + employees);

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		}
	}
}
