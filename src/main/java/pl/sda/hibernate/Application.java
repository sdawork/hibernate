package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.hibernate.dao.AnimalDAO;
import pl.sda.hibernate.dao.ContactDAO;
import pl.sda.hibernate.dao.CountryDAO;
import pl.sda.hibernate.dao.PersonDAO;
import pl.sda.hibernate.domain.*;
import pl.sda.hibernate.util.HibernateUtil;

import java.time.LocalDate;
import java.util.List;

public class Application {

	public static void main(String[] args) {
		Transaction transaction = null;
		try (
				SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				Session session = sessionFactory.openSession();
		) {
			transaction = session.beginTransaction();

			final PersonDAO personDAO = new PersonDAO(session);
			final AnimalDAO animalDAO = new AnimalDAO(session);
			final ContactDAO contactDAO = new ContactDAO(session);
			final CountryDAO countryDAO = new CountryDAO(session);

			final List<Person> polska = personDAO.findByCountryName("Polska");
			System.out.println("polska = " + polska);


			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		}
	}
}
