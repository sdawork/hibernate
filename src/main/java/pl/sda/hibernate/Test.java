package pl.sda.hibernate;

import pl.sda.hibernate.domain.Continent;

public class Test {

	public static void main(String[] args) {
		System.out.println(Continent.EUROPE.ordinal());
		System.out.println(Continent.ASIA.ordinal());
	}
}
