package pl.sda.hibernate.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String name;

	@Enumerated(EnumType.STRING)
	private Continent continent;

	@ManyToMany(mappedBy = "countries")
	private Set<Person> people = new HashSet<>();

	public Country() {
	}

	public Country(String name, Continent continent) {
		this.name = name;
		this.continent = continent;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Country country = (Country) o;
		return id == country.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Country{" +
				"id=" + id +
				", name='" + name + '\'' +
				", continent=" + continent +
				'}';
	}
}
