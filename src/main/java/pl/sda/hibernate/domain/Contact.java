package pl.sda.hibernate.domain;

import javax.persistence.*;

@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String email;
	@Embedded
	private Address address;

	@OneToOne
	@JoinColumn(name = "person_id", unique = true)
	private Person person;

	public Contact() {
	}

	public Contact(String email, Address address, Person person) {
		this.email = email;
		this.address = address;
		this.person = person;
	}

	public Contact(String email, String town, String street, String postalCode, Person person) {
		this.email = email;
		this.address = new Address(town, street, postalCode);
		this.person = person;
	}

	@Override
	public String toString() {
		return "Contact{" +
				"id=" + id +
				", email='" + email + '\'' +
				", address=" + address +
				'}';
	}
}
