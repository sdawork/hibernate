package pl.sda.hibernate.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Animal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String name;
	@Enumerated(EnumType.STRING)
	private AnimalType animalType;
	@Column(updatable = false)
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;

	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person person;


	public Animal() {
	}

	public Animal(String name, AnimalType animalType, Person person) {
		this.name = name;
		this.animalType = animalType;
		this.person = person;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@PrePersist
	public void setUpCreatedDate() {
		createdDate = LocalDateTime.now();
	}

	@PreUpdate
	public void setUpModifiedDate() {
		modifiedDate = LocalDateTime.now();
	}

	@Override
	public String toString() {
		return "Animal{" +
				"id=" + id +
				", name='" + name + '\'' +
				", animalType=" + animalType +
				", createdDate=" + createdDate +
				", modifiedDate=" + modifiedDate +
				", person=" + person +
				'}';
	}
}
