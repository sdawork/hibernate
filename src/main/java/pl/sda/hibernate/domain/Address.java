package pl.sda.hibernate.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Address implements Serializable {

	private String town;
	private String street;
	private String postalCode;

	public Address() {
	}

	public Address(String town, String street, String postalCode) {
		this.town = town;
		this.street = street;
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return "Address{" +
				"town='" + town + '\'' +
				", street='" + street + '\'' +
				", postalCode='" + postalCode + '\'' +
				'}';
	}
}
