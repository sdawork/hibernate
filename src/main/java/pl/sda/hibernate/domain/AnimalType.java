package pl.sda.hibernate.domain;

public enum AnimalType {
	CAT,
	DOG,
	PIG
}
