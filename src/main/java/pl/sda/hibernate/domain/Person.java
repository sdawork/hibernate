package pl.sda.hibernate.domain;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "CZLOWIEK")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private LocalDate dateOfBirth;
	@Transient
	private int age;

	@OneToMany(mappedBy = "person")
	private List<Animal> animals = new ArrayList<>();

	@OneToOne(mappedBy = "person")
	private Contact contact;

	@ManyToMany
	@JoinTable(
			name = "PERSON_TO_COUNTRY",
			joinColumns = {@JoinColumn(name = "person_id")},
			inverseJoinColumns = {@JoinColumn(name = "country_id")}
	)
	private Set<Country> countries = new HashSet<>();

	public Person() {
	}

	public Person(String name, LocalDate dateOfBirth) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
	}

	public void addCountry(Country country) {
		countries.add(country);
	}

	public void removeCountry(Country country) {
		countries.remove(country);
	}


	public Set<Country> getCountries() {
		return countries;
	}

	@PostLoad
	public void calculateAge() {
		age = (int) ChronoUnit.YEARS.between(dateOfBirth, LocalDate.now());
	}

	@Override
	public String toString() {
		return "Person{" +
				"id=" + id +
				", name='" + name + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", age=" + age +
				", contact=" + contact +
				'}';
	}
}
